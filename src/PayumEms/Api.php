<?php

declare(strict_types=1);

namespace Justlease\PayumEms;

use Payum\Core\Security\TokenInterface;

final class Api
{
    public const CLASSIC_MODE_PAYONLY = 'payonly';
    public const CLASSIC_MODE_FULLPAY = 'fullpay';
    public const CLASSIC_MODE_PAYPLUS = 'payplus';

    public const PAYMENT_METHOD_MASTER_CARD = 'M';
    public const PAYMENT_METHOD_VISA = 'V';
    public const PAYMENT_METHOD_AMERICAN_EXPRESS = 'A';
    public const PAYMENT_METHOD_DINERS = 'C';
    public const PAYMENT_METHOD_JCB = 'J';
    public const PAYMENT_METHOD_IDEAL = 'ideal';
    public const PAYMENT_METHOD_KLARNA = 'klarna';
    public const PAYMENT_METHOD_MAESTRO = 'MA';
    public const PAYMENT_METHOD_MAESTRO_UK = 'maestroUK';
    public const PAYMENT_METHOD_MASTER_PASS = 'masterpass';
    public const PAYMENT_METHOD_PAYPAL = 'paypal';
    public const PAYMENT_METHOD_SOFORT = 'sofort';
    public const PAYMENT_METHOD_BANCONTACT = 'BCMC';

    private const TRANSACTION_TYPE_SALE = 'sale';
    private const HASH_ALGORITHM_SHA256 = 'SHA256';

    private const API_ENDPOINT_SANDBOX = 'https://test.ipg-online.com/connect/gateway/processing';
    private const API_ENDPOINT_PRODUCTION = 'https://www.ipg-online.com/connect/gateway/processing';

    /**
     * @var string
     */
    private $storeName;

    /**
     * @var string
     */
    private $sharedSecret;

    /**
     * @var bool
     */
    private $sandbox;

    /**
     * @var string
     */
    private $classicMode;

    /**
     * @var string
     */
    private $paymentMethod;

    public function __construct(
        string $storeName,
        string $sharedSecret,
        bool $sandbox,
        string $classicMode,
        string $paymentMethod = null
    ) {
        $this->storeName = $storeName;
        $this->sharedSecret = $sharedSecret;
        $this->sandbox = $sandbox;
        $this->classicMode = $classicMode;
        $this->paymentMethod = $paymentMethod;
    }

    public function getApiEndpoint(): string
    {
        return $this->sandbox ? self::API_ENDPOINT_SANDBOX : self::API_ENDPOINT_PRODUCTION;
    }

    public function prepareOffsitePayment(
        TokenInterface $captureToken,
        TokenInterface $notifyToken,
        array $data
    ): array {
        $dateTime = date('Y:m:d-H:i:s');
        $data = array_merge($data, [
            'txntype' => self::TRANSACTION_TYPE_SALE,
            'timezone' => date_default_timezone_get(),
            'txndatetime' => $dateTime,
            'hash_algorithm' => self::HASH_ALGORITHM_SHA256,
            'hash' => $this->createRequestHash($data['chargetotal'], $data['currency'], $dateTime),
            'storename' => $this->storeName,
            'mode' => $this->classicMode,
            'paymentMethod' => $this->paymentMethod,
            'responseSuccessURL' => $captureToken->getTargetUrl(),
            'responseFailURL' => $captureToken->getTargetUrl(),
            'transactionNotificationURL' => $notifyToken->getTargetUrl(),
        ]);

        return $data;
    }

    public function verifyResponseHash(
        string $responseHash,
        string $approvalCode,
        string $chargeTotal,
        int $currency,
        string $dateTime
    ): bool {
        $expectedResponseHash = $this->createHash(
            $this->sharedSecret,
            $approvalCode,
            $chargeTotal,
            $currency,
            $dateTime,
            $this->storeName
        );

        return $responseHash === $expectedResponseHash;
    }

    public function verifyNotifyHash(
        string $responseHash,
        string $approvalCode,
        string $chargeTotal,
        int $currency,
        string $dateTime
    ): bool {
        $expectedResponseHash = $this->createHash(
            $chargeTotal,
            $this->sharedSecret,
            $currency,
            $dateTime,
            $this->storeName,
            $approvalCode
        );

        return $responseHash === $expectedResponseHash;
    }

    private function createRequestHash(string $chargeTotal, int $currency, string $dateTime): string
    {
        return $this->createHash($this->storeName, $dateTime, $chargeTotal, $currency, $this->sharedSecret);
    }

    private function createHash(...$parameters): string
    {
        return hash('sha256', bin2hex(implode('', $parameters)));
    }
}
