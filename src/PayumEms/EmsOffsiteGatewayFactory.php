<?php

declare(strict_types=1);

namespace Justlease\PayumEms;

use Justlease\PayumEms\Action\CaptureOffsiteAction;
use Justlease\PayumEms\Action\ConvertPaymentAction;
use Justlease\PayumEms\Action\NotifyAction;
use Justlease\PayumEms\Action\StatusAction;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayFactory;

class EmsOffsiteGatewayFactory extends GatewayFactory
{
    public const GATEWAY_NAME = 'ems_offsite';

    /**
     * {@inheritdoc}
     */
    protected function populateConfig(ArrayObject $config): void
    {
        $config->defaults([
            'payum.factory_name' => self::GATEWAY_NAME,
            'payum.factory_title' => 'EMS Offsite',

            'payum.action.capture' => new CaptureOffsiteAction(),
            'payum.action.notify' => new NotifyAction(),
            'payum.action.status' => new StatusAction(),
            'payum.action.convert_payment' => new ConvertPaymentAction(),
        ]);

        if (!$config['payum.api']) {
            $config['payum.default_options'] = [
                'store_name' => '',
                'shared_secret' => '',
                'sandbox' => true,
                'classic_mode' => Api::CLASSIC_MODE_PAYONLY,
                'payment_method' => null,
            ];
            $config->defaults($config['payum.default_options']);
            $config['payum.required_options'] = ['store_name', 'shared_secret', 'classic_mode'];
            $config['payum.api'] = function (ArrayObject $config) {
                $config->validateNotEmpty($config['payum.required_options']);
                return new Api(
                    (string)$config['store_name'],
                    (string)$config['shared_secret'],
                    filter_var($config['sandbox'], FILTER_VALIDATE_BOOLEAN),
                    $config['classic_mode'],
                    $config['payment_method']
                );
            };
        }
    }
}
