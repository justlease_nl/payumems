<?php

declare(strict_types=1);

namespace Justlease\PayumEms\Action;

use Payum\Core\Action\ActionInterface;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Request\GetStatusInterface;

class StatusAction implements ActionInterface
{
    /**
     * @param GetStatusInterface $request
     */
    public function execute($request): void
    {
        RequestNotSupportedException::assertSupports($this, $request);

        $model = ArrayObject::ensureArrayObject($request->getModel());

        if (!isset($model['approval_code'])) {
            $request->markNew();
            return;
        }

        $approvalCode = $model['approval_code'];
        if (substr($approvalCode, 0, 1) === 'Y') {
            $request->markCaptured();
            return;
        } elseif (substr($approvalCode, 0, 1) === 'N') {
            $request->markFailed();
            return;
        }

        $request->markUnknown();
    }

    public function supports($request): bool
    {
        return
            $request instanceof GetStatusInterface &&
            $request->getModel() instanceof \ArrayAccess;
    }
}
