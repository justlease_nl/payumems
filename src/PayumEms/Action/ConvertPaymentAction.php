<?php

declare(strict_types=1);

namespace Justlease\PayumEms\Action;

use Justlease\PayumEms\Currency;
use Justlease\PayumEms\LocalizedPayment;
use Payum\Core\Action\ActionInterface;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Model\PaymentInterface;
use Payum\Core\Request\Convert;

class ConvertPaymentAction implements ActionInterface
{
    /**
     * @param Convert $request
     * @throws \Exception
     */
    public function execute($request): void
    {
        RequestNotSupportedException::assertSupports($this, $request);

        /** @var PaymentInterface $payment */
        $payment = $request->getSource();

        $details = ArrayObject::ensureArrayObject($payment->getDetails());

        $formattedAmount = sprintf('%.2f', $payment->getTotalAmount() / 100);

        $details->replace([
            'comments' => $payment->getDescription(),
            'customerid' => $payment->getClientId(),
            'chargetotal' => $formattedAmount,
            'currency' => Currency::toNumber($payment->getCurrencyCode()),
            'oid' => $payment->getNumber(),
        ]);

        if ($payment instanceof LocalizedPayment) {
            $details['language'] = $payment->getLanguage();
        }

        $request->setResult((array)$details);
    }

    public function supports($request): bool
    {
        return
            $request instanceof Convert &&
            $request->getSource() instanceof PaymentInterface &&
            $request->getTo() == 'array';
    }
}
