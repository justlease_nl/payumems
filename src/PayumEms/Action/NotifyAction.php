<?php

declare(strict_types=1);

namespace Justlease\PayumEms\Action;

use Justlease\PayumEms\Api;
use Payum\Core\Action\ActionInterface;
use Payum\Core\ApiAwareInterface;
use Payum\Core\ApiAwareTrait;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\GatewayAwareTrait;
use Payum\Core\Reply\HttpResponse;
use Payum\Core\Request\GetHttpRequest;
use Payum\Core\Request\Notify;

/**
 * @property Api $api
 */
final class NotifyAction implements ActionInterface, GatewayAwareInterface, ApiAwareInterface
{
    use GatewayAwareTrait;
    use ApiAwareTrait;

    public function __construct()
    {
        $this->apiClass = Api::class;
    }

    /**
     * @param mixed $request
     *
     * @throws \Payum\Core\Exception\RequestNotSupportedException
     */
    public function execute($request): void
    {
        RequestNotSupportedException::assertSupports($this, $request);

        $model = ArrayObject::ensureArrayObject($request->getModel());

        $httpRequest = new GetHttpRequest();
        $this->gateway->execute($httpRequest);

        if (!isset($httpRequest->request['approval_code'])) {
            throw new HttpResponse('Invalid request', 400);
        }

        if (!$this->api->verifyNotifyHash(
            $httpRequest->request['notification_hash'],
            $httpRequest->request['approval_code'],
            $httpRequest->request['chargetotal'],
            (int)$httpRequest->request['currency'],
            $httpRequest->request['txndatetime']
        )) {
            throw new HttpResponse('Invalid notify hash', 400);
        }

        $model->replace($httpRequest->request);

        throw new HttpResponse('OK');
    }

    public function supports($request): bool
    {
        return $request instanceof Notify;
    }
}
