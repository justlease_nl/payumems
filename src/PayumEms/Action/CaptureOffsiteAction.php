<?php

declare(strict_types=1);

namespace Justlease\PayumEms\Action;

use Justlease\PayumEms\Api;
use Payum\Core\Action\ActionInterface;
use Payum\Core\ApiAwareInterface;
use Payum\Core\ApiAwareTrait;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\GatewayAwareTrait;
use Payum\Core\Reply\HttpPostRedirect;
use Payum\Core\Reply\HttpResponse;
use Payum\Core\Request\Capture;
use Payum\Core\Request\GetHttpRequest;
use Payum\Core\Security\GenericTokenFactoryAwareInterface;
use Payum\Core\Security\GenericTokenFactoryAwareTrait;

/**
 * @property Api $api
 */
final class CaptureOffsiteAction implements ActionInterface, GatewayAwareInterface, GenericTokenFactoryAwareInterface, ApiAwareInterface
{
    use ApiAwareTrait;
    use GatewayAwareTrait;
    use GenericTokenFactoryAwareTrait;

    public function __construct()
    {
        $this->apiClass = Api::class;
    }

    /**
     * @param Capture $request
     *
     * @throws \Payum\Core\Exception\RequestNotSupportedException
     * @throws \Payum\Core\Reply\ReplyInterface
     */
    public function execute($request): void
    {
        RequestNotSupportedException::assertSupports($this, $request);

        $model = ArrayObject::ensureArrayObject($request->getModel());

        $httpRequest = new GetHttpRequest();
        $this->gateway->execute($httpRequest);

        if (isset($httpRequest->request['approval_code'])) {
            if (!$this->api->verifyResponseHash(
                $httpRequest->request['response_hash'],
                $httpRequest->request['approval_code'],
                $httpRequest->request['chargetotal'],
                (int)$httpRequest->request['currency'],
                $httpRequest->request['txndatetime']
            )) {
                throw new HttpResponse('Invalid response hash', 400);
            }

            $model->replace($httpRequest->request);
            return;
        }

        if (isset($model['approval_code'])) {
            return;
        }

        $captureToken = $request->getToken();

        $notifyToken = $this->tokenFactory->createNotifyToken(
            $captureToken->getGatewayName(),
            $captureToken->getDetails()
        );

        $data = $this->api->prepareOffsitePayment(
            $captureToken,
            $notifyToken,
            $model->toUnsafeArray()
        );

        throw new HttpPostRedirect($this->api->getApiEndpoint(), $data);
    }

    public function supports($request): bool
    {
        return $request instanceof Capture
            && $request->getModel() instanceof \ArrayAccess;
    }
}
