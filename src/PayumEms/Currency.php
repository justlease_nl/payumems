<?php

declare(strict_types=1);

namespace Justlease\PayumEms;

final class Currency
{
    private const CURRENCIES = [
        'EUR' => 978,
    ];

    public static function toNumber(string $code): int
    {
        if (!isset(self::CURRENCIES[$code])) {
            throw new \Exception('Currency not supported: ' . $code);
        }

        return self::CURRENCIES[$code];
    }
}
