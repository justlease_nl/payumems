<?php

declare(strict_types=1);

namespace Justlease\PayumEms;

interface LocalizedPayment
{
    public function getLanguage(): string;
}
