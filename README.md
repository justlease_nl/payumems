# PayumEms

This package provides integration with [EMS Pay](https://emspay.nl) for Payum.

## Installation

```sh
$ composer require justlease/payum-ems
```

## Usage

If you're using Symfony, consider using [justlease/payum-ems-bundle](https://bitbucket.org/justlease_nl/payumemsbundle) to configure the gateway.

Otherwise, see the example below on how to add the gateway.

```php
<?php

use Payum\Core\GatewayFactoryInterface;
use Justlease\PayumEms\EmsOffsiteGatewayFactory;

/** @var \Payum\Core\PayumBuilder $payumBuilder */
$payumBuilder->addGatewayFactory('ems_offsite', function (array $config, GatewayFactoryInterface $gatewayFactory) {
    return new EmsOffsiteGatewayFactory($config, $gatewayFactory);
});

$payumBuilder->addGateway('ems_offsite', [
    'factory' => 'ems_offsite',
    'store_name' => '123456789',
    'shared_secret' => 'mysharedsecret',
    'sandbox' => true,
    'classic_mode' => \Justlease\PayumEms\Api::CLASSIC_MODE_PAYONLY,
    'payment_method' => \Justlease\PayumEms\Api::PAYMENT_METHOD_IDEAL,
]);

```
